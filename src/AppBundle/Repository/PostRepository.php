<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PostRepository extends EntityRepository
{
    public function getAll($offset, $limit, $sort)
    {
        $qb = $this->createQueryBuilder('p');

        // On ajoute des jointures pour les enfants
        $query = $qb->select('p')
            //->setFirstResult($offset)
            //->setMaxResults($limit)
           // ->orderBy('p.title')
            //->leftJoin('a.image', 'i')
            //->where('a.locale= :locale')
            //->setParameter('locale', $locale)
        ;


        if ($offset != "") {
            $qb->setFirstResult($offset);
        }

        if ($limit != "") {
            $qb->setMaxResults($limit);
        }

        if (in_array($sort, ['asc', 'desc'])) {
            $qb->orderBy('p.title', $sort);
        }

        //$qb->orderBy('p.title');

        return $query->getQuery()
            ->getResult();
    }
}
