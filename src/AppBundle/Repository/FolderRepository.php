<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query;

/**
 * Class FolderRepository
 * @package AppBundle\Repository
 */
class FolderRepository extends EntityRepository
{

	/**
     * Get a folder from an id, all hydrated
     *
     * @param integer $id
     * @return Folder
     */
    public function getHydratedFolderById($id)
    {
        $qb = $this->createQueryBuilder('f');

        $query = $qb
            ->select('f')
            ->innerJoin('ADXCmsBundle:Node', 'n', 'WITH', 'n.id = f.id')
            ->where('f.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
        ;
        //$query->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true);
        return $query->getOneOrNullResult();
    }


    /**
     * get Folders
     */
    public function getFolders()
    {   
        $qb = $this->createQueryBuilder('f');
        $query = $qb
            ->select('f')
            //->innerJoin('ADX\CmsBundle:Node', 'n', 'WITH', 'f.id = n.id')
            /*
            ->where('n.visible = :v')
            ->andWhere('n.type = :t')
            ->setParameters(
	        	array(
	        		't' => 'folder',
	        		'v' => 1
	        	)
	        )*/
            ->getQuery()
        ;
        //$query->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true);

        //var_dump($query->getResult());
        return $query->getResult();
    }

}
