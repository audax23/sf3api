<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query;

/**
 * CategoryRepository
 *
 */
class CategoryRepository extends TranslatableRepository
{

    public function getAll()
    {
        $qb = $this->createQueryBuilder('c');

        $query = $qb
            ->select('c')
            ->orderBy('c.root, c.lft', 'ASC')
        ;

        return $query->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker')
            ->getResult();
    }


    /**
     *
     *
     */
    public function getCategoryByParentAndLocale($parent, $locale){

        $qb = $this->createQueryBuilder('c');

        $query = $qb
            ->select('c')
            ->where('c.parent = :parent')
            ->setParameter('parent', $parent->getId())
            ->orderBy('c.root, c.lft', 'ASC')
            ->getQuery()
        ;
        $query->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');

        return $this->getResult($qb, $locale);
    }


    /**
     * Returns all categories ordered by title ascendant.
     *
     * @return \AppBundle\Entity\category
     */
    public function getCategories()
    {
        return $this->findBy([], [ 'title' => 'ASC' ]);
    }
}
