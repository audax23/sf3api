<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;

use Nelmio\ApiDocBundle\Annotation as Doc;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use AppBundle\Entity\Category;
use AppBundle\Entity\Article;
use AppBundle\Form\Type\ArticleType;

/**
 * Article controller.
 *
 *
 */
class ArticleController extends Controller
{

    /**
     *
     * @Rest\View()
     * @Rest\Get("/articles")
     * @QueryParam(name="offset", requirements="\d+", default="", description="Index de début de la pagination")
     * @QueryParam(name="limit", requirements="\d+", default="", description="Index de fin de la pagination")
     * @QueryParam(name="sort", requirements="(asc|desc)", nullable=true, description="Ordre de tri (basé sur le nom)")
     *
     * @ApiDoc(section="Articles",
     *     description="Récupère la liste des articles",
     *     output= { "class"=Article::class, "collection"=true, "groups"={"article"} }
     * )
     */
    public function getArticlesAction(Request $request, ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        $sort = $paramFetcher->get('sort');

        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository("AppBundle:Article")->getAll($offset, $limit, $sort);

        if ($articles === null) {
            return new View("there are no Articles exist", Response::HTTP_NOT_FOUND);
        }

        /* @var $articles Article[] */
        // Création d'une vue FOSRestBundle
        $view = View::create($articles);
        $view->setFormat('json')
            ->setTemplate("AppBundle:Article:index.html.twig")
            ->setTemplateVar('articles')
        ;

        return $view;
        //return $articles;

    } // "get_articles"            [GET] /articles

    /**
     *
     * @Rest\View()
     * @Rest\Get("/articles/{id}")
     * @ApiDoc(section="Articles")
     */
    public function getArticleAction(Request $request)
    {
        $article = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Article')
            ->find($request->get('id'));
        if ($article === null) {
            return new View("there are no articles exist", Response::HTTP_NOT_FOUND);
        }

        $view = View::create($article, 200);
        $view
            ->setTemplate("AppBundle:Article:show.html.twig")
            ->setTemplateVar('article')
        ;

        return $view;

        /*
        $view = $this->view($article, 200)
            ->setTemplate("AppBundle:Article:show.html.twig")
            ->setTemplateVar('article')
        ;

        return $this->handleView($view);
        */
    } // "get_article"             [GET] /articles/{id}

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/articles")
     * @ApiDoc(section="Articles")
     */
    public function postArticleAction(Request $request)
    {

        $data = new Article;
        $title = $request->get('title');
        $slug = $request->get('slug');
        if(empty($title) || empty($slug))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $data->setTitle($title);
        $data->setSlug($slug);
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        return new View("User Added Successfully", Response::HTTP_OK);

    }

    /**
     * Creates a form to create a Category entity.
     *
     * @param Article $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Article $entity)
    {
        $form = $this->createForm(ArticleType::class, $entity, array(
            'action' => $this->generateUrl('get_articles'),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Create'));

        return $form;
    }


    /**
     * @Rest\View()
     * @Rest\Put("/articles/{id}")
     * @ApiDoc(section="Articles")
     */
    public function putArticleAction(Article $article)
    {
        $article = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Article')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $article Article */

        if (empty($article)) {
            return new JsonResponse(['message' => 'Article not found'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(ArticleType::class, $article);

        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');

            // l'entité vient de la base, donc le merge n'est pas nécessaire.
            // il est utilisé juste par soucis de clarté
            $em->merge($article);
            $em->flush();
            return $article;
        } else {
            return $form;
        }

    }// "put_article"             [PUT] /articles/{id}


    /**
     * @Rest\View()
     * @Rest\Patch("/articles/{id}")
     * @ApiDoc(section="Articles")
     */
    public function patchArticleAction(Request $request)
    {
        return $this->updateArticle($request, false);
    }

    private function updateArticle(Request $request, $clearMissing)
    {
        $article = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Article')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $article Article */

        if (empty($article)) {
            return new JsonResponse(['message' => 'Article not found'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(ArticleType::class, $article);

        // Le paramètre false dit à Symfony de garder les valeurs dans notre
        // entité si l'utilisateur n'en fournit pas une dans sa requête
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($article);
            $em->flush();
            return $article;
        } else {
            return $form;
        }
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/articles/{id}")
     * @ApiDoc(section="Articles")
     */
    public function removeArticleAction(Article $article)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('AppBundle:Article')->find($article);
        /* @var $article Article */
        if ($article) {
            $em->remove($article);
            $em->flush();
        }
    }

}