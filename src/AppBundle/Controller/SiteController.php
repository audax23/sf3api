<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations\Version;

use Nelmio\ApiDocBundle\Annotation as Doc;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use AppBundle\Entity\Category;
use AppBundle\Entity\Site;
use AppBundle\Form\Type\SiteType;


/**
 * Site controller.
 *
 *
 */
class SiteController extends Controller
{

    /**
     *
     * @Rest\View()
     * @Rest\Get("/sites")
     * @ApiDoc(section="Sites")
     */
    public function getSitesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $sites = $em->getRepository("AppBundle:Site")->getAll();

        //var_dump($sites);

        if ($sites === null) {
            return new View("there are no Sites exist", Response::HTTP_NOT_FOUND);
        }

        /* @var $sites Site[] */

        // Création d'une vue FOSRestBundle
        $view = View::create($sites);
        $view->setFormat('json');

        return $view;
        //return $articles;

    } // "get_articles"            [GET] /articles

    /**
     *
     * @Rest\View()
     * @Rest\Get("/sites/{id}")
     * @ApiDoc(section="Sites")
     */
    public function getSiteAction(Request $request)
    {
        $site = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Site')
            ->find($request->get('id'));
        if ($site === null) {
            return new View("there are no articles exist", Response::HTTP_NOT_FOUND);
        }

        $view = $this->view($site, 200)
            ->setTemplate("AppBundle:Site:show.html.twig")
            ->setTemplateVar('article')
        ;

        return $this->handleView($view);

    } // "get_article"             [GET] /articles/{id}

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/sites")
     * @ApiDoc(section="Sites")
     */
    public function postSiteAction(Request $request)
    {

        $data = new Site();
        $title = $request->get('title');
        $slug = $request->get('slug');
        if(empty($title) || empty($slug))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $data->setTitle($title);
        $data->setSlug($slug);
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        return new View("Site Added Successfully", Response::HTTP_OK);

    }

    /**
     * Creates a form to create a Category entity.
     *
     * @param Site $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Site $entity)
    {
        $form = $this->createForm(SiteType::class, $entity, array(
            'action' => $this->generateUrl('api_site_new'),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Create'));

        return $form;
    }


    /**
     * @Rest\View()
     * @Rest\Put("/sites/{id}")
     * @ApiDoc(section="Sites")
     */
    public function putSiteAction(Request $request, Site $site)
    {
        $site = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Site')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $site Site */

        if (empty($site)) {
            return new JsonResponse(['message' => 'Site not found'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(SiteType::class, $site);

        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');

            // l'entité vient de la base, donc le merge n'est pas nécessaire.
            // il est utilisé juste par soucis de clarté
            $em->merge($site);
            $em->flush();
            return $site;
        } else {
            return $form;
        }

    }// "put_article"             [PUT] /articles/{id}


    /**
     * @Rest\View()
     * @Rest\Patch("/sites/{id}")
     * @ApiDoc(section="Sites")
     */
    public function patchSiteAction(Request $request)
    {
        return $this->updateSite($request, false);
    }

    private function updateSite(Request $request, $clearMissing)
    {
        $site = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Site')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $site Site */

        if (empty($site)) {
            return new JsonResponse(['message' => 'Site not found'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(SiteType::class, $site);

        // Le paramètre false dit à Symfony de garder les valeurs dans notre
        // entité si l'utilisateur n'en fournit pas une dans sa requête
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($site);
            $em->flush();
            return $site;
        } else {
            return $form;
        }
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/sites/{id}")
     * @ApiDoc(section="Sites")
     */
    public function removeSiteAction(Site $site)
    {
        $em = $this->getDoctrine()->getManager();
        $site = $em->getRepository('AppBundle:Site')->find($site);
        /* @var $site Site */
        if ($site) {
            $em->remove($site);
            $em->flush();
        }
    }

}