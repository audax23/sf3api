<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\Post;
use AppBundle\Form\PostType;

use GuzzleHttp\Client;

/**
 * @Route("/default")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }

    /**
     * @Route("/{id}/front", name="view")
     * TODO changer l'appel pour récupérer l'utilisateur
     * TODO Gérer les exceptions
     */
    public function viewtAction($id)
    {
        // Create a client with a base URI
        $client = new \GuzzleHttp\Client();

        $user = json_decode(
            $client->request('GET', 'http://sf3api.local/api/articles')->getBody()->getContents()
        );

        if($user){
            $listingJSON = json_decode($client->request('GET', 'http://sf3api.local/api/articles')->getBody()->getContents());
            $oneJSON = json_decode($client->request('GET', 'http://sf3api.local/api/articles/'.$id)->getBody()->getContents());
        }else{
            throw new Exception();
        }
        return $this->render('default/json.html.twig', [
            'listingJSON' =>$listingJSON,
            'oneJSON' =>$oneJSON
        ]);
    }

}
