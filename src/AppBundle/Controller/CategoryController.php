<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations\Version;

use Nelmio\ApiDocBundle\Annotation as Doc;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use AppBundle\Entity\Category;
use AppBundle\Form\Type\CategoryType;


/**
 * Category controller.
 *
 *
 */
class CategoryController extends Controller
{

    /**
     * @Rest\Get("/categories", name="api_categories")
     * @Rest\View()
     * @ApiDoc(section="Categories")
     */
    public function getCategoriesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('AppBundle:Category')->getAll();
        //return $this->get('app_core.repository.category')->getCategories();
        //$json = $this->container->get('serializer')->serialize($categories, 'json');

        //return new Response($json, 200, array('application/json'));

        if ($categories === null) {
            return new View("there are no categories exist", Response::HTTP_NOT_FOUND);
        }

        /* @var $categories Category[] */

        // Création d'une vue FOSRestBundle
        $view = View::create($categories);
        $view->setFormat('json');

        return $view;
    }

    /**
     *
     * @Rest\View()
     * @Rest\Get("/categories/{id}")
     * @ApiDoc(section="Categories")
     */
    public function getCategoryAction(Request $request)
    {
        $category = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Category')
            ->find($request->get('id'));
        if ($category === null) {
            return new View("there are no categories exist", Response::HTTP_NOT_FOUND);
        }

        $view = $this->view($category, 200)
            ->setTemplate("AppBundle:Category:show.html.twig")
            ->setTemplateVar('category')
        ;

        return $this->handleView($view);

    } // "get_article"             [GET] /articles/{id}

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/categories")
     * @ApiDoc(section="Categories")
     */
    public function postCategoryAction(Request $request)
    {

        $data = new Category;
        $title = $request->get('title');
        $slug = $request->get('slug');
        if(empty($title) || empty($slug))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $data->setTitle($title);
        $data->setSlug($slug);
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        return new View("Category Added Successfully", Response::HTTP_OK);

    }

    /**
     * Creates a form to create a Category entity.
     *
     * @param Category $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Category $entity)
    {
        $form = $this->createForm(CategoryType::class, $entity, array(
            'action' => $this->generateUrl('api_article_new'),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Create'));

        return $form;
    }


    /**
     * @Rest\View()
     * @Rest\Put("/categories/{id}")
     * @ApiDoc(section="Categories")
     */
    public function putCategoryAction(Request $request)
    {
        $category = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Category')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $category Category */

        if (empty($category)) {
            return new JsonResponse(['message' => 'Category not found'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(CategoryType::class, $category);

        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');

            // l'entité vient de la base, donc le merge n'est pas nécessaire.
            // il est utilisé juste par soucis de clarté
            $em->merge($category);
            $em->flush();
            return $category;
        } else {
            return $form;
        }

    }// "put_article"             [PUT] /articles/{id}


    /**
     * @Rest\View()
     * @Rest\Patch("/categories/{id}")
     * @ApiDoc(section="Categories")
     */
    public function patchCategoryAction(Request $request)
    {
        return $this->updateCategory($request, false);
    }

    private function updateCategory(Request $request, $clearMissing)
    {
        $category = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Category')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $category Category */

        if (empty($category)) {
            return new JsonResponse(['message' => 'Category not found'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(CategoryType::class, $category);

        // Le paramètre false dit à Symfony de garder les valeurs dans notre
        // entité si l'utilisateur n'en fournit pas une dans sa requête
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($category);
            $em->flush();
            return $category;
        } else {
            return $form;
        }
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/categories/{id}")
     * @ApiDoc(section="Categories")
     */
    public function removeCategoryAction(Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->find($category);
        /* @var $category Category */
        if ($category) {
            $em->remove($category);
            $em->flush();
        }
    }





}
