<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\View\View;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\FileParam;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use AppBundle\Entity\User;
use AppBundle\Form\Type\UserType;

/**
 * Users controller.
 *
 *
 */
class UserController extends Controller
{

    /**
     *
     * @Rest\View()
     * @Rest\Get("/users")
     * @ApiDoc(section="Users")
     */
    public function getUsersAction()
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('AppBundle:User')->getAll();

        if ($users === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }

        /*
        $view = $this->view($users, 200)
            ->setTemplate("AppBundle:User:index.html.twig")
            ->setTemplateVar('users')
        ;
        */

        /* @var $users User[] */

        // Création d'une vue FOSRestBundle
        $view = View::create($users);
        $view->setFormat('json');

        //return $view;
        return $view;


    } // "get_users"            [GET] /users




    /**
     *
     * @Rest\View()
     * @Rest\Get("/users/{id}")
     * @ApiDoc(section="Users")
     */
    public function getUserAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->getUserById($request->get('id'));

        /* @var $user User */

        if (empty($user)) {
            return new JsonResponse(['message' => 'User not found'], Response::HTTP_NOT_FOUND);
        }

        return $user;

    } // "get_user"             [GET] /users/{id}



    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/users")
     * @ApiDoc(section="Users")
     */
    public function postUserAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->submit($request->request->all()); // Validation des données

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $form->get('password')->getData());

            $user->setPassword($password);
            $user->addRole('ROLE_ADMIN');
            $user->setEnabled(true);
            $em->persist($user);
            $em->flush();
            return $user;
        } else {
            return $form;
        }
    } // "post_user"             [POST] /users


    /**
     * @Rest\View()
     * @Rest\Put("/users/{id}")
     * @ApiDoc(section="Users")
     */
    public function updateUserAction(Request $request)
    {
        $user = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:User')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $user User */

        if (empty($user)) {
            return new JsonResponse(['message' => 'User not found'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(UserType::class, $user);

        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $form->get('password')->getData());

            $user->setPassword($password);
            $user->addRole('ROLE_ADMIN');
            // l'entité vient de la base, donc le merge n'est pas nécessaire.
            // il est utilisé juste par soucis de clarté
            $em->merge($user);
            $em->flush();
            return $user;
        } else {
            return $form;
        }
    }


    /**
     * @Rest\View()
     * @Rest\Patch("/users/{id}")
     * @ApiDoc(section="Users")
     */
    public function patchUserAction(Request $request)
    {
        return $this->updateUser($request, false);
    }

    private function updateUser(Request $request, $clearMissing)
    {
        $user = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:User')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $user User */

        if (empty($user)) {
            return new JsonResponse(['message' => 'User not found'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(UserType::class, $user);

        // Le paramètre false dit à Symfony de garder les valeurs dans notre
        // entité si l'utilisateur n'en fournit pas une dans sa requête
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($user);
            $em->flush();
            return $user;
        } else {
            return $form;
        }
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/users/{id}")
     * @ApiDoc(section="Users")
     */
    public function removeUsersAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')
            ->find($request->get('id'));
        /* @var $user User */

        $em->remove($user);
        $em->flush();

        return new JsonResponse(['message' => 'User deleted'], Response::HTTP_FOUND);
    }


}
