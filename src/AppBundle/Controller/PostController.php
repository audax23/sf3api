<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;

use Nelmio\ApiDocBundle\Annotation as Doc;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use AppBundle\Entity\Post;
use AppBundle\Form\Type\PostType;

/**
 * Post controller.
 *
 */
class PostController extends Controller
{
    /**
     * @Rest\Get("/posts")
     * @Rest\View()
     * @QueryParam(name="offset", requirements="\d+", default="", description="Index de début de la pagination")
     * @QueryParam(name="limit", requirements="\d+", default="", description="Index de fin de la pagination")
     * @QueryParam(name="sort", requirements="(asc|desc)", nullable=true, description="Ordre de tri (basé sur le nom)")
     * @Doc\ApiDoc(section="Posts",
     *     description="Récupère la liste des lieux de l'application",
     *     output= { "class"=Post::class, "collection"=true, "groups"={"post"} }
     * )
     */
    public function getPostsAction(Request $request, ParamFetcher $paramFetcher)
    {
        /*
        return new JsonResponse([
            new Post("Tour Eiffel", "5 Avenue Anatole France, 75007 Paris"),
            new Post("Mont-Saint-Michel", "50170 Le Mont-Saint-Michel"),
            new Post("Château de Versailles", "Place d'Armes, 78000 Versailles"),
        ]);
        */
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        $sort = $paramFetcher->get('sort');

        $em = $this->getDoctrine()->getManager();
        $posts = $em->getRepository("AppBundle:Post")->getAll($offset, $limit, $sort);

        if ($posts === null) {
            return new View("there are no Articles exist", Response::HTTP_NOT_FOUND);
        }

        /* @var $posts Post[] */

        // Création d'une vue FOSRestBundle
        $view = View::create($posts);
        $view->setFormat('json');
        //$view->setFormat('html');
        $view->setTemplate("AppBundle:Post:index.html.twig")
            ->setTemplateVar('posts')
            ->setData($posts)
        ;

        return $view;
        //return $articles;

    }


    /**
     * @Rest\Get("/posts/new")
     * @Rest\View()
     * @Doc\ApiDoc(section="Posts")
     */
    public function newAction(Request $request)
    {

        /*
        $post = new Post();
        $form = $this->createCreateForm($post);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirect($this->generateUrl('category_show', array('id' => $category->getId())));
        }
        return $this->render('AppBundle:Post:new.html.twig', [
            'post' => $post,
            'form' => $form->createView()
        ]);
        */




        $em = $this->getDoctrine()->getManager();

        for($i=0; $i<=50; $i++){

            $post[$i] = new Post();

            $post[$i]->setTitle('Chaussures'.$i);
            $post[$i]->setDescription('Desc Chaussures'.$i);
            $post[$i]->setSlug('Chaussures'.$i);

            $post[$i]->translate('fr_CA')->setTitle('Chaussures'.$i);
            $post[$i]->translate('fr_CA')->setDescription('Desc Chaussures'.$i);
            $post[$i]->translate('fr_CA')->setSlug('Chaussures'.$i);

            $post[$i]->translate('fr_BE')->setTitle('Chaussures'.$i);
            $post[$i]->translate('fr_BE')->setDescription('Desc Chaussures'.$i);
            $post[$i]->translate('fr_BE')->setSlug('Chaussures'.$i);

            $post[$i]->translate('en_US')->setTitle('Shoes en_US'.$i);
            $post[$i]->translate('en_US')->setDescription('Desc Shoes en_US'.$i);
            $post[$i]->translate('en_US')->setSlug('Shoes en_US'.$i);

            $em->persist($post[$i]);

            // In order to persist new translations, call mergeNewTranslations method, before flush
            $post[$i]->mergeNewTranslations();

            $em->flush($post[$i]);


        }




        return new Response('insert ok');
        //$post->translate('en')->getName();


    }



    /**
     * Creates a form to create a Category entity.
     *
     * @param Post $post The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Post $post)
    {
        $form = $this->createForm(PostType::class, $post, array(
            'action' => $this->generateUrl('post_create'),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Create'));

        return $form;
    }

    /**
     * @Rest\Get("/posts/{id}")
     * @Rest\View()
     * @Doc\ApiDoc(section="Posts")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('AppBundle:Post')->find($id);

        if (!$post) {
            throw $this->createNotFoundException('Unable to find Post entity.');
        }
        /* @var $posts Post[] */

        // Création d'une vue FOSRestBundle
        $view = View::create($post);
        $view->setFormat('json');
        //$view->setFormat('html');
        $view->setTemplate("AppBundle:Post:show.html.twig")
            ->setTemplateVar('post')
            ->setData($post)
        ;

        return $view;

        /*return $this->render('AppBundle:Post:show.html.twig', [
            'post'      => $post
        ]);*/

    }

}