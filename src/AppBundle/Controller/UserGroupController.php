<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\Version;

use Nelmio\ApiDocBundle\Annotation as Doc;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use AppBundle\Entity\UserGroup;
use AppBundle\Form\Type\UserGroupType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;



/**
 * Groups controller.
 *
 */
class UserGroupController extends Controller
{
    /**
     * @Rest\View()
     * @Rest\Get("/groups")
     * @ApiDoc(section="Groups")
     */
    public function getGroupsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $groups = $em->getRepository('AppBundle:UserGroup')->getAll();

        if ($groups === null) {
            return new View("there are no groups exist", Response::HTTP_NOT_FOUND);
        }

        /* @var $groups UserGroup[] */

        // Création d'une vue FOSRestBundle
        //$view = View::create($users);
        //$view->setFormat('json');

        //return $view;
        return $groups;

    }

    /**
     *
     * @Rest\View()
     * @Rest\Get("/groups/{id}")
     * @ApiDoc(section="Groups")
     */
    public function getGroupAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository('AppBundle:UserGroup')->getGroupById($request->get('id'));

        /* @var $group UserGroup */

        if (empty($group)) {
            return new JsonResponse(['message' => 'Group not found'], Response::HTTP_NOT_FOUND);
        }

        return $group;

    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/groups")
     * @ApiDoc(section="Groups")
     */
    public function postGroupAction(Request $request)
    {
        $group = new UserGroup();
        $form = $this->createForm(UserGroupType::class, $group);

        $form->submit($request->request->all()); // Validation des données

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $group->addRole('ROLE_ADMIN');
            $em->persist($group);
            $em->flush();
            return $group;
        } else {
            return $form;
        }
    }

    /**
     * @Rest\View()
     * @Rest\Put("/groups/{id}")
     * @ApiDoc(section="Groups")
     */
    public function updateGroupAction(Request $request)
    {
        $group = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:UserGroup')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $group UserGroup */

        if (empty($group)) {
            return new JsonResponse(['message' => 'Group not found'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(UserGroupType::class, $group);

        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $group->addRole('ROLE_ADMIN');
            // l'entité vient de la base, donc le merge n'est pas nécessaire.
            // il est utilisé juste par soucis de clarté
            $em->merge($group);
            $em->flush();
            return $group;
        } else {
            return $form;
        }
    }


    /**
     * @Rest\View()
     * @Rest\Patch("/groups/{id}")
     * @ApiDoc(section="Groups")
     */
    public function patchGroupAction(Request $request)
    {
        return $this->updateGroup($request, false);
    }

    private function updateGroup(Request $request, $clearMissing)
    {
        $group = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:UserGroup')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $group UserGroup */

        if (empty($group)) {
            return new JsonResponse(['message' => 'Group not found'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(UserGroupType::class, $group);

        // Le paramètre false dit à Symfony de garder les valeurs dans notre
        // entité si l'utilisateur n'en fournit pas une dans sa requête
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($group);
            $em->flush();
            return $group;
        } else {
            return $form;
        }
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/groups/{id}")
     * @ApiDoc(section="Groups")
     */
    public function removeGroupAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $group = $em->getRepository('AppBundle:UserGroup')
            ->find($request->get('id'));
        /* @var $group UserGroup */

        $em->remove($group);
        $em->flush();

        return new JsonResponse(['message' => 'Group deleted'], Response::HTTP_FOUND);
    }


}
