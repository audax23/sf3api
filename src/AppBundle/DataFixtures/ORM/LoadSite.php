<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;

use AppBundle\Entity\Site;
use AppBundle\Entity\Folder;
use AppBundle\Entity\Page;
use AppBundle\Entity\Article;

class LoadSite extends AbstractFixture implements OrderedFixtureInterface
{
  // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
  public function load(ObjectManager $manager)
  {
    // Liste à ajouter
    
    $sites = array(
        'site1' => array(
            'name' => 'Site France',  
            'title' => 'Site France',  
            'url'=>'site-fr',
            'type'=>'site',
            'parent'=>'',
            'locale'=>'fr',
            'visible'=>1,
            'published'=>1,
            'sitename' => 'Site France',
            'sitedomain' => 'Site France'
        ),
        'site2' => array(
            'name' => 'Site Royaume-Uni',  
            'title' => 'Site Royaume-Uni',  
            'url'=>'site-gb',
            'type'=>'site',
            'parent'=>'',
            'locale'=>'en_GB',
            'visible'=>1,
            'published'=>1,
            'sitename' => 'Site Royaume-Uni',
            'sitedomain' => 'Site Royaume-Uni'
        ),
        'site3'=> array (
            'name' => 'Site Belgique',  
            'title' => 'Site Belgique',  
            'url'=>'site-be',
            'type'=>'site',
            'parent'=>'',
            'locale'=>'fr_BE',
            'visible'=>1,
            'published'=>1,
            'sitename' => 'Site Belgique',
            'sitedomain' => 'Site Belgique'
        ),
        'site4'=> array (
            'name' => 'Site Suisse',  
            'title' => 'Site Suisse',  
            'url'=>'site-ch',
            'type'=>'site',
            'parent'=>'',
            'locale'=>'fr_CH',
            'visible'=>1,
            'published'=>1,
            'sitename' => 'Site Suisse',
            'sitedomain' => 'Site Suisse'
        ),
        'site5'=> array (
            'name' => 'Site Italie',  
            'title' => 'Site Italie',  
            'url'=>'site-it',
            'type'=>'site',
            'parent'=>'',
            'locale'=>'it_IT',
            'visible'=>1,
            'published'=>1,
            'sitename' => 'Site Italie',
            'sitedomain' => 'Site Italie'
        )
    );
    

    $folders = array(
      'folder1' => array(
          'name' => 'Fr',  
          'title' => 'FR',  
          'url'=>'fr',
          'type'=>'folder',
          'parent'=>'site-1',
          'locale'=>'fr',
          'visible'=>1,
          'published'=>1,
          'foldertitle' => 'fr',
          'foldercontent' => 'fr'
      ),
      'folder2' => array(
          'name' => 'en',  
          'title' => 'Anglais Royaume-Uni',  
          'url'=>'en',
          'type'=>'folder',
          'parent'=>'site-2',
          'locale'=>'en_GB',
          'visible'=>1,
          'published'=>1,
          'foldertitle' => 'en_GB',
          'foldercontent' => 'en_GB'
      ),
      'folder3'=> array (
          'name' => 'Fr Be',  
          'title' => 'Français Belge',  
          'url'=>'be',
          'type'=>'folder',
          'parent'=>'site-3',
          'locale'=>'fr_BE',
          'visible'=>1,
          'published'=>1,
          'foldertitle' => 'fr_BE',
          'foldercontent' => 'fr_BE'
      ),
      'folder4'=> array (
          'name' => 'Fr CH',  
          'title' => 'Français Suisse',  
          'url'=>'ch',
          'type'=>'folder',
          'parent'=>'site-4',
          'locale'=>'fr_CH',
          'visible'=>1,
          'published'=>1,
          'foldertitle' => 'fr_CH',
          'foldercontent' => 'fr_CH'
      ),
      'folder5'=> array (
          'name' => 'it',  
          'title' => 'it Italie',  
          'url'=>'it',
          'type'=>'folder',
          'parent'=>'site-5',
          'locale'=>'it_IT',
          'visible'=>1,
          'published'=>1,
          'foldertitle' => 'it_IT',
          'foldercontent' => 'it_IT'
      )
    );


    $pages = array(
      'page1' => array(
          'name' => 'page fr',  
          'title' => 'page fr',  
          'url'=>'page-fr',
          'type'=>'page',
          'parent'=>'folder-1',
          'locale'=>'fr',
          'visible'=>1,
          'published'=>1,
          'pagetitle' => 'fr',
          'pagecontent' => 'fr'
      ),
      'page2' => array(
          'name' => 'page en',  
          'title' => 'Anglais Royaume-Uni',  
          'url'=>'gb',
          'type'=>'page',
          'parent'=>'folder-2',
          'locale'=>'en_GB',
          'visible'=>1,
          'published'=>1,
          'pagetitle' => 'en_GB',
          'pagecontent' => 'en_GB'
      ),
      'page3'=> array (
          'name' => 'page Fr Be',  
          'title' => 'page Français Belge',  
          'url'=>'be',
          'type'=>'page',
          'parent'=>'folder-3',
          'locale'=>'fr_BE',
          'visible'=>1,
          'published'=>1,
          'pagetitle' => 'page fr_BE',
          'pagecontent' => 'page fr_BE'
      ),
      'page4'=> array (
          'name' => 'page Fr CH',  
          'title' => 'page Français Suisse',  
          'url'=>'ch',
          'type'=>'page',
          'parent'=>'folder-4',
          'locale'=>'fr_CH',
          'visible'=>1,
          'published'=>1,
          'pagetitle' => 'page fr_CH',
          'pagecontent' => 'page fr_CH'
      ),
      'page5'=> array (
          'name' => 'page it',  
          'title' => 'page it Italie',  
          'url'=>'it',
          'type'=>'page',
          'parent'=>'folder-5',
          'locale'=>'it_IT',
          'visible'=>1,
          'published'=>1,
          'pagetitle' => 'page it_IT',
          'pagecontent' => 'page it_IT'
      )
    );
    
    $articles = array(
      'article1' => array(
          'name' => 'article fr',  
          'title' => 'article fr',  
          'url'=>'article-fr',
          'type'=>'article',
          'parent'=>'page-1',
          'locale'=>'fr',
          'visible'=>1,
          'published'=>1,
          'articlename' => 'article fr',
          'articlecontent' => 'article fr'
      ),
      'article2' => array(
          'name' => 'article2 fr',  
          'title' => 'article2 fr',  
          'url'=>'article2-fr',
          'type'=>'article',
          'parent'=>'page-1',
          'locale'=>'fr',
          'visible'=>1,
          'published'=>1,
          'articlename' => 'article2 fr',
          'articlecontent' => 'article2 fr'
      ),
      'article3' => array(
          'name' => 'article en',  
          'title' => 'article Anglais Royaume-Uni',  
          'url'=>'article-gb',
          'type'=>'article',
          'parent'=>'page-2',
          'locale'=>'en_GB',
          'visible'=>1,
          'published'=>1,
          'articlename' => 'article en_GB',
          'articlecontent' => 'article en_GB'
      ),
      'article4'=> array (
          'name' => 'article Fr Be',  
          'title' => 'article Français Belge',  
          'url'=>'article-be',
          'type'=>'article',
          'parent'=>'page-3',
          'locale'=>'fr_BE',
          'visible'=>1,
          'published'=>1,
          'articlename' => 'article fr_BE',
          'articlecontent' => 'article fr_BE'
      ),
      'article5'=> array (
          'name' => 'article Fr CH',  
          'title' => 'article Français Suisse',  
          'url'=>'article-ch',
          'type'=>'article',
          'parent'=>'page-4',
          'locale'=>'fr_CH',
          'visible'=>1,
          'published'=>1,
          'articlename' => 'article fr_CH',
          'articlecontent' => 'article fr_CH'
      ),
      'article6'=> array (
          'name' => 'article it',  
          'title' => 'article it Italie',  
          'url'=>'article-it',
          'type'=>'article',
          'parent'=>'page-5',
          'locale'=>'it_IT',
          'visible'=>1,
          'published'=>1,
          'articlename' => 'article it_IT',
          'articlecontent' => 'article it_IT'
      )
    );





    for($i=1; $i<=5; $i++ )
    {
      // On crée des valeurs sites, type de noeud le plus haut
      $site = new Site();
      //$site->setName($sites['site'.$i]['name']);
      $site->setTitle($sites['site'.$i]['title']);
      $site->setUrl($sites['site'.$i]['url']);
      $site->setType($sites['site'.$i]['type']);
      $site->setParent($sites['site'.$i]['parent'] = null);
      $site->setLocale($sites['site'.$i]['locale']);
      $site->setVisible($sites['site'.$i]['visible']);
      $site->setPublished($sites['site'.$i]['published']);
      $site->setSitename($sites['site'.$i]['sitename']);
      $site->setDomain($sites['site'.$i]['sitedomain']);

      echo $sites['site'.$i]['name'];
      // On la persiste
      $manager->persist($site);
      $this->setReference('site-'.$i, $site);

      // On crée le folder
      $folder = new Folder();
      $folder->setName($folders['folder'.$i]['name']);
      $folder->setTitle($folders['folder'.$i]['title']);
      $folder->setUrl($folders['folder'.$i]['url']);
      $folder->setType($folders['folder'.$i]['type']);
      $folder->setLocale($folders['folder'.$i]['locale']);
      $folder->setParent($manager->merge($this->getReference('site-'.$i)));
      $folder->setVisible($folders['folder'.$i]['visible']);
      $folder->setPublished($folders['folder'.$i]['published']);
      $folder->setFoldertitle($folders['folder'.$i]['foldertitle']);
      $folder->setFoldercontent($folders['folder'.$i]['foldercontent']);
      
      echo $folders['folder'.$i]['name'];
      // On la persiste
      $manager->persist($folder);
      $this->setReference('folder-'.$i, $folder);

      
    }

    for($j=1; $j<=5; $j++ )
    {
        // On crée la page
        $page = new Page();
        $page->setName($pages['page'.$j]['name']);
        $page->setTitle($pages['page'.$j]['title']);
        $page->setUrl($pages['page'.$j]['url']);
        $page->setType($pages['page'.$j]['type']);
        $page->setLocale($pages['page'.$j]['locale']);
        $page->setParent($manager->merge($this->getReference('folder-'.$j)));
        $page->setVisible($pages['page'.$j]['visible']);
        $page->setPublished($pages['page'.$j]['published']);
        $page->setPagetitle($pages['page'.$j]['pagetitle']);
        $page->setPagecontent($pages['page'.$j]['pagecontent']);

        echo $pages['page'.$j]['name'];
        // On la persiste
        $manager->persist($page);
        $this->setReference('page-'.$j, $page);

        
    }

    for($k=1; $k<=6; $k++ )
    {
        // On crée Article
        $article = new Article();
        $article->setName($articles['article'.$k]['name']);
        $article->setTitle($articles['article'.$k]['title']);
        $article->setUrl($articles['article'.$k]['url']);
        $article->setType($articles['article'.$k]['type']);
        $article->setLocale($articles['article'.$k]['locale']);
        $article->setParent($this->getReference($articles['article'.$k]['parent'])->getId());
        $article->setVisible($articles['article'.$k]['visible']);
        $article->setPublished($articles['article'.$k]['published']);
        $article->setArtname($articles['article'.$k]['articlename']);
        $article->setArtcontent($articles['article'.$k]['articlecontent']);

        echo $this->getReference($articles['article'.$k]['parent']);
        // On la persiste
        $manager->persist($article);
        $this->setReference('article-'.$k, $article);
    }
/*
    for($i=1; $i<=5; $i++ )
    {
      // On crée le folder
      $folder = new Folder();
      $folder->setName($folders['folder'.$i]['name']);
      $folder->setTitle($folders['folder'.$i]['title']);
      $folder->setUrl($folders['folder'.$i]['url']);
      $folder->setType($folders['folder'.$i]['type']);
      $folder->setLocale($folders['folder'.$i]['locale']);
      $folder->setParent($manager->merge($this->getReference('site-'.$i)));
      $folder->setVisible($folders['folder'.$i]['visible']);
      $folder->setPublished($folders['folder'.$i]['published']);
      $folder->setFoldertitle($folders['folder'.$i]['foldertitle']);
      $folder->setFoldercontent($folders['folder'.$i]['foldercontent']);
      
      echo $folders['folder'.$i]['name'];
      // On la persiste
      $manager->persist($folder);
      $this->addReference('folder-'.$i, $folder);
    }



    for($i=1; $i<=5; $i++ )
    {
      // On crée la page
      $page = new Page();
      $page->setName($pages['page'.$i]['name']);
      $page->setTitle($pages['page'.$i]['title']);
      $page->setUrl($pages['page'.$i]['url']);
      $page->setType($pages['page'.$i]['type']);
      $page->setLocale($pages['page'.$i]['locale']);
      $page->setParent($manager->merge($this->getReference('folder-'.$i)));
      $page->setVisible($pages['page'.$i]['visible']);
      $page->setPublished($pages['page'.$i]['published']);
      $page->setPagetitle($pages['page'.$i]['pagetitle']);
      $page->setPagecontent($pages['page'.$i]['pagecontent']);

      echo $pages['page'.$i]['name'];
      // On la persiste
      $manager->persist($page);
      $this->addReference('page-'.$i, $page);
    }
    */
    // On déclenche l'enregistrement de toutes les catégories
    $manager->flush();
  }

  public function getOrder()
  {
      return 4;
  }
}