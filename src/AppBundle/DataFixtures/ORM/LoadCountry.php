<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Country;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;

class LoadCountry extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $countries = array(
            "country1" => array(
                'name' => 'FR'
            ),
            "country2" => array(
                'name' => 'CH'
            ),
            "country3" => array(
                'name' => 'IT'
            ),
            "country4" => array(
                'name' => 'SI'
            ),
            "country5" => array(
                'name' => 'DE'
            ),
            "country6" => array(
                'name' => 'CA'
            ),
        );

        for($i=1; $i<=sizeof($countries); $i++ ) {

            $country = new Country();
            $country->setName($countries['country' . $i]['name']);

            $manager->persist($country);
            $this->setReference('country-'.$i, $country);
        }
        $manager->flush();

    }

    public function getOrder()
    {
        return 1;
    }

}