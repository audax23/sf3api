<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="folder")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FolderRepository")
 */
class Folder extends Topic
{

    /**
     * @var string
     * 
     * @ORM\Column(name="foldertitle", type="string", length=255)
     */
    private $foldertitle;

    /**
     * @var string
     * 
     * @ORM\Column(name="foldercontent", type="text")
     */
    private $foldercontent;


    /**
     * Set pagetitle
     *
     * @param string $foldertitle
     * @return Folder
     */
    public function setFoldertitle($foldertitle)
    {
        $this->foldertitle = $foldertitle;

        return $this;
    }

    /**
     * Get foldertitle
     *
     * @return string 
     */
    public function getFoldertitle()
    {
        return $this->foldertitle;
    }

    /**
     * Set foldercontent
     *
     * @param string $foldercontent
     * @return Folder
     */
    public function setFoldercontent($foldercontent)
    {
        $this->foldercontent = $foldercontent;

        return $this;
    }

    /**
     * Get foldercontent
     *
     * @return string 
     */
    public function getFoldercontent()
    {
        return $this->foldercontent;
    }
    
}
