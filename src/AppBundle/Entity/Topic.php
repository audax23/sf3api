<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
#use AppBundle\Entity\Traits\SEO;
use AppBundle\Entity\User;
/**
 * Topic
 * @ORM\Table(name="topic")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TopicRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="topic_type", type="string")
 * @ORM\DiscriminatorMap({"topic" = "Topic", "folder" = "Folder", "page" = "Page"})
 */
 class Topic
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

     /**
      * Trait SEO
      */
     #use SEO;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable= true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable= true)
     */
    protected $title;

     /**
      * @Gedmo\Slug(fields={"title"})
      * @ORM\Column(length=128)
      */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable= true)
     * @Assert\NotBlank(message="validation.topic.description")
     */
    protected $description;

    /**
     * @ORM\Column(name="root", type="integer", nullable=true)
     */
    protected $root;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", nullable=true)
     */
    protected $visible;

    /**
     * @var boolean
     *
     * @ORM\Column(name="homepage", type="boolean", nullable=true)
     */
    protected $isHomepage;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=6, nullable=true)
     */
    protected $locale;

    /**
    * @var string
    *
    * @ORM\Column(name="type", type="string", length=255)
    */
     protected $type;

    /**
    * @var string
    *
    * @ORM\Column(name="lvl", type="integer", nullable = true)
    */
    protected $lvl;

    /**
    * @var string
    *
    * @ORM\Column(name="position", type="integer", nullable = true)
    */
    protected $position;

    /**
    * @var array
    *
    * @ORM\Column(name="menus", type="array")
    */
    private $menus;

    /**
    * @ORM\ManyToOne(targetEntity="Template")
    */
    protected $template;

    /**
     * @ORM\ManyToOne(targetEntity="Country", cascade={"persist"})
     */
    protected $country;

    /**
     * @ORM\ManyToOne(targetEntity="Site", cascade={"persist"})
     */
    protected $site;

    /**
     * @ORM\ManyToOne(targetEntity="Topic", cascade={"persist"}, inversedBy="childrens")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $topicParent;

     /**
      * @ORM\OneToMany(targetEntity="Topic", cascade={"persist"}, mappedBy="topicParent", cascade={"remove","persist"})
      */
     protected $childrens;

     /**
      * @var \DateTime $createdAt
      *
      * @Gedmo\Timestampable(on="create")
      * @ORM\Column(name="createdAt", type="datetime")
      */
     private $createdAt;

     /**
      * @var \DateTime $updatedAt
      *
      * @Gedmo\Timestampable(on="update")
      * @ORM\Column(name="updatedAt", type="datetime")
      */
     private $updatedAt;

     /**
      * @var \DateTime $deletedAt
      *
      * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
      */
     private $deletedAt;

     /**
      * @var \DateTime $startDate
      * @ORM\Column(name="startDate", type="datetime")
      */
     private $startDate;

     /**
      * @var \DateTime $endDate
      * @ORM\Column(name="endDate", type="datetime", nullable=true)
      */
     private $endDate;

     /**
      * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\User")
      */
     protected $user;

     /**
      * Constructor
      */
     public function __construct()
     {
         $this->childrens = new \Doctrine\Common\Collections\ArrayCollection();
     }

     /**
      * @return string
      */
     public function __toString()
     {
         return $this->name;
     }

     /**
      * Clone Object with children
      */
     public function __clone()
     {
         if ($this->id) {
             $childrens = $this->getChildrens();
             $this->childrens = new ArrayCollection();
             foreach ($childrens as $child) {
                 $cloneChild = clone $child;
                 $this->childrens->add($cloneChild);
                 $cloneChild->setTopicParent($this);

             }
         }
     }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Topic
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Topic
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Topic
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return Topic
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string 
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set root
     *
     * @param integer $root
     * @return Topic
     */
    public function setRoot($root)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return integer 
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Topic
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set isHomepage
     *
     * @param boolean $isHomepage
     *
     * @return Topic
     */
    public function setIsHomepage($isHomepage)
    {
        $this->isHomepage = $isHomepage;

        return $this;
    }

    /**
     * Get isHomepage
     *
     * @return boolean
     */
    public function getIsHomepage()
    {
        return $this->isHomepage;
    }

    public static function getNodeType(){}

    /**
     * Set country
     *
     * @param \ADX\CmsBundle\Entity\Country $country
     *
     * @return Topic
     */
    public function setCountry(\AppBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \AppBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set site
     *
     * @param \AppBundle\Entity\Site $site
     *
     * @return Topic
     */
    public function setSite(\AppBundle\Entity\Site $site = null)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return \AppBundle\Entity\Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set topicParent
     *
     * @param \AppBundle\Entity\Topic $topicParent
     *
     * @return Topic
     */
    public function setTopicParent(\AppBundle\Entity\Topic $topicParent = null)
    {
        $this->topicParent = $topicParent;

        return $this;
    }

    /**
     * Get topicParent
     *
     * @return \AppBundle\Entity\Topic
     */
    public function getTopicParent()
    {
        return $this->topicParent;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Topic
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return Topic
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Topic
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set menus
     *
     * @param array $menus
     *
     * @return Topic
     */
    public function setMenus($menus)
    {
        $this->menus = $menus;

        return $this;
    }

    /**
     * Get menus
     *
     * @return array
     */
    public function getMenus()
    {
        return $this->menus;
    }

    /**
     * Add children
     *
     * @param \AppBundle\Entity\Topic $children
     *
     * @return Topic
     */
    public function addChildren(\AppBundle\Entity\Topic $children)
    {
        $this->childrens[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \AppBundle\Entity\Topic $children
     */
    public function removeChildren(\AppBundle\Entity\Topic $children)
    {
        $this->childrens->removeElement($children);
    }

    /**
     * Get childrens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildrens()
    {
        return $this->childrens;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Topic
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set template
     *
     * @param \AppBundle\Entity\Template $template
     *
     * @return Topic
     */
    public function setTemplate(\AppBundle\Entity\Template $template = null)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return \AppBundle\Entity\Template
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
    * @return \DateTime
    */
    public function getCreatedAt()
    {
     return $this->createdAt;
    }

    /**
    * @param \DateTime $createdAt
    */
    public function setCreatedAt($createdAt)
    {
     $this->createdAt = $createdAt;
    }

    /**
    * @return \DateTime
    */
    public function getUpdatedAt()
    {
     return $this->updatedAt;
    }

    /**
    * @param \DateTime $updatedAt
    */
    public function setUpdatedAt($updatedAt)
    {
     $this->updatedAt = $updatedAt;
    }

    /**
    * @return \DateTime
    */
    public function getDeletedAt()
    {
     return $this->deletedAt;
    }

    /**
    * @param \DateTime $deletedAt
    */
    public function setDeletedAt($deletedAt)
    {
     $this->deletedAt = $deletedAt;
    }

    /**
    * @return \DateTime
    */
    public function getStartDate()
    {
     return $this->startDate;
    }

    /**
    * @param \DateTime $startDate
    */
    public function setStartDate($startDate)
    {
     $this->startDate = $startDate;
    }

    /**
    * @return \DateTime
    */
    public function getEndDate()
    {
     return $this->endDate;
    }

    /**
    * @param \DateTime $endDate
    */
    public function setEndDate($endDate)
    {
     $this->endDate = $endDate;
    }

     /**
      * @return mixed
      */
     public function getUser()
     {
         return $this->user;
     }

     /**
      * @param mixed $user
      */
     public function setUser($user)
     {
         $this->user = $user;
     }


}
