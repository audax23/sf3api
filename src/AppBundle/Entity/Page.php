<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="page")
 * @ORM\Entity()
 */
class Page extends Topic
{

    /**
     * @var string
     * 
     * @ORM\Column(name="pagetitle", type="string", length=255)
     */
    private $pagetitle;

    /**
     * @var string
     * 
     * @ORM\Column(name="pagecontent", type="text")
     */
    private $pagecontent;


    /**
     * Set pagetitle
     *
     * @param string $pagetitle
     * @return Page
     */
    public function setPagetitle($pagetitle)
    {
        $this->pagetitle = $pagetitle;

        return $this;
    }

    /**
     * Get pagetitle
     *
     * @return string 
     */
    public function getPagetitle()
    {
        return $this->pagetitle;
    }

    /**
     * Set pagecontent
     *
     * @param string $pagecontent
     * @return Page
     */
    public function setPagecontent($pagecontent)
    {
        $this->pagecontent = $pagecontent;

        return $this;
    }

    /**
     * Get pagecontent
     *
     * @return string 
     */
    public function getPagecontent()
    {
        return $this->pagecontent;
    }
    
}
