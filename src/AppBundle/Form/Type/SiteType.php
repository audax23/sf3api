<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Intl\Intl as Intl;
use Symfony\Component\Intl\Locale as Locale;

/**
 * Class SiteType
 * @package AppBundle\Form\Type
 */
class SiteType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sitename', TextType::class, array('label' => 'site.sitename'))
            ->add('domain', TextType::class, array('label' => 'site.domain'))
            ->add('templatePath', ChoiceType::class, array(
                    'choices' => array(
                        'Default LTR' => 'adx_ltr',
                        'Default RTL' => 'adx_rtl'
                    )
                )
            );

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $entity = $event->getData();
            $form = $event->getForm();

            if($entity) {
                $form->add('language', ChoiceType::class, array(
                        'label'     => 'site.language',
                        'choices'   => $this->getLocaleByCountry($entity->getCountry()->getName()),
                        'choice_label' => function ($value, $key, $index) {
                            return Intl::getLocaleBundle()->getLocaleName($value);
                        },
                        'multiple'  => true
                    )
                )
                ->add('defaultLocale', ChoiceType::class, array(
                    'label' => 'site.default_locale',
                    'choices' => $this->getLocaleByCountry($entity->getCountry()->getName())
                ))
                ->add('submit', SubmitType::class, array('label' => 'Valid'));
            }
        });

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'            => 'AppBundle\Entity\Site',
            'attr'                  => ['id' => 'adx_cms_site'],
            'translation_domain'    => 'site'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'adx_cms_site';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adx_cms_site';
    }

    /**
     * @param $name
     * @return array
     */
    public static function getLocaleByCountry($name)
    {
        $leslocales = array();
        $locales =  Intl::getLocaleBundle()->getLocales();

        //dump(Intl::getLocaleBundle()->getLocaleNames($name));

        foreach($locales as $locale)
        {
            //&& strlen($locale) > 2
            if (strtoupper(substr($locale, strlen($locale)-strlen($name), 2)) == $name)
            {
                $leslocales[$locale] = $locale;
            }
        }
        return $leslocales;
    }

}
