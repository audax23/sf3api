<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\DataTransformer\SiteTransformer;
use AppBundle\Form\DataTransformer\CountryTransformer;

/**
 * Class FolderType
 * @package AppBundle\Form\Type
 */
class FolderType extends TopicType
{

    protected $em;

    protected $session;

    public function __construct(EntityManager $em, Session $session)
    {
        $this->em = $em;
        $this->session = $session;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('foldertitle', TextType::class, array('label' => 'topic.folder.title'))
            ->add('foldercontent', TextType::class, array('label' => 'topic.folder.name'))
            /*->add('isHomepage', ChoiceType::class, array(
                'label'=>'topic.page.isHomepage',
                'choices' => array(
                    'Oui' => '1',
                    'Non' => '0'
                ),
                'multiple' => false,
                'expanded' => true
            ))
            ->add('template', EntityType::class, array(
                'class'         => 'ADXCmsBundle:Template',
                'choice_label'      => 'name',
                'required'      => false
                'query_builder' => function(TemplateRepository $tr)
                {
                    return $tr->createQueryBuilder('tr')
                              ->where('tr.folder = :folder')
                              ->setParameter('folder', $this->session->get('site')->getTemplatePath());
                }
            ))*/
            ->add('country', HiddenType::class, array('data'=>$this->session->get('country')->getId()))
            ->add('site', HiddenType::class, array('data'=>$this->session->get('site')->getId()))
            ->add('locale', HiddenType::class, array('data'=>$this->session->get('locale_topic') ? $this->session->get('locale_topic') : $this->session->get('site')->getDefaultLocale() ))
            //->add('submit', SubmitType::class, array('label' => 'Valid'))
        ;

        /** Modèle transformers */
        $builder->get('country')->addModelTransformer(new CountryTransformer($this->em, $this->session));
        $builder->get('site')->addModelTransformer(new SiteTransformer($this->em, $this->session));



    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Topic',
            'attr' => ['id' => 'adx_cms_folder', 'class' => 'smart-form'],
            'translation_domain' => 'topic'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'adx_cms_folder';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adx_cms_folder';
    }
}
