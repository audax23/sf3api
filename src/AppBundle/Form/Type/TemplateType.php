<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class TemplateType
 * @package AppBundle\Form\Type
 */
class TemplateType extends AbstractType
{

    /**
     * @var string
     */
    private $templatePath;

    /**
     * @param $templatePath
     */
    public function __construct($templatePath) {
        $this->templatePath = $templatePath;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('name', TextType::class, array('label' => 'template.name'))
            ->add('folder', ChoiceType::class, array(
                'choices' => $this->templatePath
            ))
            ->add('contenu', TextareaType::class, array(
                'label'=> 'template.contenu',
                'attr'=> array('rows'=>20,'class'=>'area-code'))
            )
            ->add('submit', SubmitType::class);

    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver$resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Template',
            'attr' => ['id' => 'adx_cms_template'],
            'translation_domain' => 'template'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adx_cms_template';
    }
}
