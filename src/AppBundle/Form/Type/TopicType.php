<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class TopicType
 * @package AppBundle\Form\Type
 */
class TopicType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('title', TextType::class)
            ->add('description', TextareaType::class, array(
                'attr'=> array('class'=>'ckeditor'),
                'label'=>'topic.description',
                'required'=>false
                )
            )
            ->add('startDate', DateType::class, array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                // do not render as type="date", to avoid HTML5 date pickers
                'html5' => false,
                'attr' => ['class'=>'js-datepicker', 'data-provide'=>'datepicker', 'data-date-format'=>'yyyy/mm/dd'],
            ))
            ->add('endDate', DateType::class, array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                // do not render as type="date", to avoid HTML5 date pickers
                'html5' => false,
                'attr' => ['class'=>'js-datepicker', 'data-provide'=>'datepicker', 'data-date-format'=>'yyyy/mm/dd'],
                'required' => false
            ))
            ->add('visible', ChoiceType::class, array(
                'label'=>'topic.visible',
                'choices' => array(
                    '1' => 'Oui',
                    '0' => 'Non'
                ),
                'multiple' => false,
                'expanded' => true,
                'data'    => '0',
            ));

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $entity = $event->getData();
            $form = $event->getForm();

            if($entity->getId()) {
                $form->add('slug', TextType::class);
            }
        });

    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Topic',
            'attr' => ['id' => 'adx_cms_topic', 'class' => 'smart-form'],
            'translation_domain' => 'topic'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adx_cms_topic';
    }
}

